#!/bin/bash

# If not running interactively, don't do anything
case $- in
	*i*) ;;
	*) return;;
esac

# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
for file in ~/.{bash_prompt,bash_profile,aliases,functions,path,dockerfunc,extra,exports}; do
	if [[ -r "$file" ]] && [[ -f "$file" ]]; then
		# shellcheck source=/dev/null
		source "$file"
	fi
done
unset file

test -f ~/.profile-kieran && . ~/.profile-kieran # Silly stuff goes here

alias gl='git log --graph --oneline --all --decorate --pretty="%C(bold)%ad%C(reset) %C(yellow)%h%C(reset) %an %C(blue)%s" --date=format:"%y/%m/%d"'

alias pstorm=/opt/PhpStorm*/bin/phpstorm.sh

export EDITOR='pstorm'
export VISUAL='pstorm'

pstorm > /dev/null 2>&1 &

if ! grep -q idea.io.coarse /opt/PhpStorm*/bin/idea.properties
	then
	echo "idea.io.coarse.ts=true" >> /opt/PhpStorm*/bin/idea.properties
fi
